<!--
SPDX-FileCopyrightText: 2023 Justus Perlwitz
SPDX-FileCopyrightText: 2024 Justus Perlwitz

SPDX-License-Identifier: MIT
-->

# Show how long since a break or whatever has started

Useful when going over the given break time and wanting to know how much
one has gone over.

# DONE

## Allow passing commands along

I want to be able to start pomoglorbo with the tags I want to use for
timewarror.
