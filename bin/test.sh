#!/usr/bin/env sh

# SPDX-FileCopyrightText: 2023 Justus Perlwitz
# SPDX-FileCopyrightText: 2024 Justus Perlwitz
#
# SPDX-License-Identifier: MIT

set -e
if ! ruff format --check .; then
    echo "Formatting with ruff format"
    if ! ruff format .; then
        echo "Failed running ruff format"
        exit 1
    fi
fi

echo "ruff format --check finished"

if ! ruff check .; then
    echo "Attempting to fix with ruff --fix"
    if ! ruff check --fix .; then
        echo "Failed running ruff --fix"
        exit 1
    fi
    if ! ruff check .; then
        echo "There are still some issues. Please fix and run $0 again"
    fi
fi

echo "ruff finished"

if mypy .; then
    echo "mypy finished"
else
    echo "mypy did not finish successfully"
    exit 1
fi

if reuse lint; then
    echo "reuse lint finished"
else
    echo "There was a problem running reuse lint"
fi
