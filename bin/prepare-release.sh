#!/usr/bin/env bash
# SPDX-FileCopyrightText: 2024 Justus Perlwitz
#
# SPDX-License-Identifier: MIT
# Prepare a new version to be release
set -e
poetry install
version="$(poetry run src/pomoglorbo/cli/__init__.py --version)"
read -p "about to run git tag $version. Continue? (c-c to exit)"
git tag "$version"
poetry build
read -p "about to run poetry publish. Continue? (c-c to exit)"
poetry publish
read -p "about to run git push --tags. Continue? (c-c to exit)"
git push --tags
