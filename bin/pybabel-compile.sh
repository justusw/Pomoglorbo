#!/usr/bin/env sh
# SPDX-FileCopyrightText: 2024 Justus Perlwitz
#
# SPDX-License-Identifier: MIT
set -e
pybabel compile --directory=src/pomoglorbo/messages

