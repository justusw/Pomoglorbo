# SPDX-FileCopyrightText: 2023 Justus Perlwitz
# SPDX-FileCopyrightText: 2024 Justus Perlwitz
#
# SPDX-License-Identifier: MIT

[build-system]
requires = ["poetry-core>=1.0.0"]
build-backend = "poetry.core.masonry.api"

[tool.poetry.scripts]
pomoglorbo = "pomoglorbo.cli:main"

[tool.poetry]
name = "pomoglorbo"
version = "2024.6.23.1"
keywords = ["tomato", "pomodoro", "pomoglorbo", "pydoro", "timer", "work"]
authors = [
    "Bhathiya Perera <jadogg.coder@gmail.com>",
    "Justus Perlwitz <justus@jwpconsulting.net>",
]
maintainers = [
    "Justus Perlwitz <justus@jwpconsulting.net>",
]
description = "Terminal Pomodoro timer"
license = "MIT"
readme = "README.md"
classifiers = [
    "License :: OSI Approved :: MIT License",
    "Programming Language :: Python :: 3",
    "Topic :: Office/Business :: Scheduling",
    "Topic :: Utilities",
]
repository = "https://codeberg.org/justusw/Pomoglorbo"

[tool.poetry.dependencies]
python = "^3.10"
prompt-toolkit = "~3.0"
xdg-base-dirs = "~6.0"
playsound3 = "2.2.1"
poetry-core = "^1.8.1"

[tool.poetry.group.test.dependencies]
mypy = "^1.10"
pyright = "^1.1.367"
ruff = "0.4.4"

[tool.poetry.group.dev.dependencies]
babel = "^2.15.0"
yappi = "^1.6.0"
snakeviz = "^2.2.0"

[tool.mypy]
files = ["src", "setup.py"]
exclude = "build"
# https://mypy.readthedocs.io/en/stable/existing_code.html#introduce-stricter-options
# Start off with these
warn_unused_configs = true
warn_redundant_casts = true
warn_unused_ignores = true

# Getting these passing should be easy
strict_equality = true
extra_checks = true

# Strongly recommend enabling this one as soon as you can
check_untyped_defs = true

# These shouldn't be too much additional work, but may be tricky to
# get passing if you use a lot of untyped libraries
disallow_untyped_decorators = true
disallow_subclassing_any = true
disallow_any_generics = true

# These next few are various gradations of forcing use of type annotations
disallow_untyped_calls = true
disallow_incomplete_defs = true
disallow_untyped_defs = true

# This one isn't too hard to get passing, but return on investment is lower
no_implicit_reexport = true

# This one can be tricky to get passing if you use a lot of untyped libraries
warn_return_any = true

strict = true

[[tool.mypy.overrides]]
module = [
    "playsound3",
]
ignore_missing_imports = true

[tool.ruff]
[tool.ruff.lint]
select = [
    "E",
    "F",
    "I",
    "W",
]
fixable = ["ALL"]

[tool.pyright]
typeCheckingMode = "strict"
