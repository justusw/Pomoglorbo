# SPDX-FileCopyrightText: 2021-2023 Bhatihya Perera
# SPDX-FileCopyrightText: 2023 Justus Perlwitz
# SPDX-FileCopyrightText: 2024 Justus Perlwitz
#
# SPDX-License-Identifier: MIT
from typing import (
    Callable,
    Optional,
)

from prompt_toolkit.application import Application
from prompt_toolkit.filters import Condition
from prompt_toolkit.key_binding import (
    KeyBindings,
    KeyPressEvent,
)
from prompt_toolkit.key_binding.bindings.focus import (
    focus_next,
    focus_previous,
)
from prompt_toolkit.layout import (
    ConditionalContainer,
    Dimension,
    FormattedTextControl,
    HSplit,
    Layout,
    VSplit,
    Window,
)
from prompt_toolkit.widgets import (
    Box,
    Button,
)

from pomoglorbo.cli.util import exit_clicked
from pomoglorbo.cli.util import gettext_lazy as _
from pomoglorbo.core.tomato import (
    make_tomato,
    tomato_interact,
)
from pomoglorbo.types import (
    Configuration,
    Tomato,
    TomatoLayout,
    UserInterface,
)

EventCallback = Callable[[KeyPressEvent], None]


Actions = dict[str, EventCallback]


def get_key_bindings(
    config: Configuration,
    actions: Actions,
) -> KeyBindings:
    kb = KeyBindings()
    for action, keys in config.key_bindings.items():
        cb: Optional[EventCallback] = actions.get(action)
        if not cb:
            continue
        if not isinstance(keys, str):
            raise ValueError(f"Expected {keys} to be str")
        for key in keys.split(","):
            key = key.strip()
            kb.add(key)(cb)
    return kb


def create_layout(tomato: Tomato) -> TomatoLayout:
    btn_start = Button("Start", handler=lambda: tomato_interact(tomato, "start"))
    buttons: list[Button] = [
        btn_start,
        Button(_("Pause"), handler=lambda: tomato_interact(tomato, "pause")),
        Button(_("Reset"), handler=lambda: tomato_interact(tomato, "reset")),
        Button(_("Reset All"), handler=lambda: tomato_interact(tomato, "reset_all")),
        Button(_("Exit"), handler=exit_clicked),
    ]

    @Condition
    def help_visible() -> bool:
        return tomato.show_help

    help_content = _("""Help
====
Start      | {start}
Pause      | {pause}
Reset      | {reset}
Reset All  | {reset_all}
Help       | {help}
Focus Prev | {focus_previous}
Focus Next | {focus_next}
Exit       | {exit_clicked}
""").format(**tomato.config.key_bindings)
    helpwindow = ConditionalContainer(
        content=Window(FormattedTextControl(help_content)),
        filter=help_visible,
    )

    # All the widgets for the UI.
    warning_display = FormattedTextControl(focusable=False)
    last_cmd_display = FormattedTextControl(focusable=False)
    status = FormattedTextControl(focusable=False)
    text_area = FormattedTextControl(focusable=False, show_cursor=False)
    text_width = Dimension(max=40)
    root_container = Box(
        VSplit(
            [
                HSplit(buttons, padding=1),
                HSplit(
                    [
                        Window(content=text_area, width=text_width, wrap_lines=True),
                        helpwindow,
                    ],
                    padding=1,
                ),
            ],
            padding=4,
        )
    )

    return TomatoLayout(
        layout=Layout(container=root_container, focused_element=btn_start),
        text_area=text_area,
        status=status,
        helpwindow=helpwindow,
        warning_display=warning_display,
        last_cmd_display=last_cmd_display,
    )


def make_user_interface(config: Configuration) -> UserInterface:
    tomato = make_tomato(config)

    layout = create_layout(tomato)

    actions: Actions = {
        "focus_next": focus_next,
        "focus_previous": focus_previous,
        "exit_clicked": lambda _: exit_clicked(),
        "start": lambda _: tomato_interact(tomato, "start"),
        "pause": lambda _: tomato_interact(tomato, "pause"),
        "reset": lambda _: tomato_interact(tomato, "reset"),
        "reset_all": lambda _: tomato_interact(tomato, "reset_all"),
        "help": lambda _: tomato_interact(tomato, "toggle_help"),
    }

    application: Application[object] = Application(
        layout=layout.layout,
        key_bindings=get_key_bindings(tomato.config, actions),
        full_screen=True,
        refresh_interval=1,
    )
    return UserInterface(
        tomato=tomato,
        layout=layout,
        config=config,
        application=application,
    )
