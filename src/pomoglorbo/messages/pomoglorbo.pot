# Translations template for Pomoglorbo.
# SPDX-FileCopyrightText: 2024 Justus Perlwitz
# SPDX-License-Identifier: MIT
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Pomoglorbo 2024.6.23.1\n"
"Report-Msgid-Bugs-To: justus@jwpconsulting.net\n"
"POT-Creation-Date: 2024-06-23 23:19+0900\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.15.0\n"

#: src/pomoglorbo/cli/__init__.py
msgid "Playing alarm once..."
msgstr ""

#: src/pomoglorbo/cli/__init__.py
msgid "Playing alarm twice..."
msgstr ""

#: src/pomoglorbo/cli/__init__.py
msgid "Have a nice day"
msgstr ""

#: src/pomoglorbo/cli/layout.py
msgid "Pause"
msgstr ""

#: src/pomoglorbo/cli/layout.py
msgid "Reset"
msgstr ""

#: src/pomoglorbo/cli/layout.py
msgid "Reset All"
msgstr ""

#: src/pomoglorbo/cli/layout.py
msgid "Exit"
msgstr ""

#: src/pomoglorbo/cli/layout.py
msgid ""
"Help\n"
"====\n"
"Start      | {start}\n"
"Pause      | {pause}\n"
"Reset      | {reset}\n"
"Reset All  | {reset_all}\n"
"Help       | {help}\n"
"Focus Prev | {focus_previous}\n"
"Focus Next | {focus_next}\n"
"Exit       | {exit_clicked}\n"
msgstr ""

#: src/pomoglorbo/cli/render.py
msgid "Press [start] to continue"
msgstr ""

#: src/pomoglorbo/cli/render.py
msgid "Press [start] to begin"
msgstr ""

#: src/pomoglorbo/cli/render.py
msgid "1 set completed"
msgid_plural "{sets} sets completed"
msgstr[0] ""
msgstr[1] ""

#: src/pomoglorbo/core/config.py
msgid "Pomoglorbo: TUI Pomodoro Technique Timer"
msgstr ""

#: src/pomoglorbo/core/config.py
msgid ""
"Please refer to the terms of the LICENSES included with Pomoglorbo.\n"
"\n"
"Curious about how to use Pomoglorbo? Read my blog post: "
"https://www.justus.pw/posts/2024-06-18-try-pomoglorbo.html\n"
"\n"
"Stay up to date with changes: https://codeberg.org/justusw/Pomoglorbo\n"
"\n"
"Reach out to me if you have any questions: justus@jwpconsulting.net\n"
msgstr ""

#: src/pomoglorbo/core/config.py
msgid "Mute alarm"
msgstr ""

#: src/pomoglorbo/core/config.py
msgid "Play alarm and exit"
msgstr ""

#: src/pomoglorbo/core/config.py
msgid "Display version and exit"
msgstr ""

#: src/pomoglorbo/core/config.py
msgid "Custom audio file for alarm"
msgstr ""

#: src/pomoglorbo/core/config.py
msgid ""
"Use a different config file. Overrides POMOGLORBO_CONFIG_FILE environment"
" variable. Default is \"$XDG_CONFIG_HOME/pomoglorbo/config.ini\"."
msgstr ""

#: src/pomoglorbo/core/config.py
msgid ""
"Append these arguments to external command invocation when starting the "
"next Pomodoro"
msgstr ""

#: src/pomoglorbo/core/states.py
msgid "Ready"
msgstr ""

#: src/pomoglorbo/core/states.py
msgid "Work mode"
msgstr ""

#: src/pomoglorbo/core/states.py
msgid "Paused"
msgstr ""

#: src/pomoglorbo/core/states.py
msgid "Small break"
msgstr ""

#: src/pomoglorbo/core/states.py
msgid "Long break"
msgstr ""

#: src/pomoglorbo/core/util.py
msgid "{}{minutes}min {seconds:02}s remaining"
msgstr ""

#: src/pomoglorbo/core/util.py
msgid "{minutes:02}:{seconds:02}"
msgstr ""

#: ../../../../nix/store/3diglh51dp3wbayycz40wixk145i03w2-python3-3.10.14/lib/python3.10/argparse.py
msgid "usage: "
msgstr ""

#: ../../../../nix/store/3diglh51dp3wbayycz40wixk145i03w2-python3-3.10.14/lib/python3.10/argparse.py
msgid ".__call__() not defined"
msgstr ""

#: ../../../../nix/store/3diglh51dp3wbayycz40wixk145i03w2-python3-3.10.14/lib/python3.10/argparse.py
#, python-format
msgid "unknown parser %(parser_name)r (choices: %(choices)s)"
msgstr ""

#: ../../../../nix/store/3diglh51dp3wbayycz40wixk145i03w2-python3-3.10.14/lib/python3.10/argparse.py
#, python-format
msgid "argument \"-\" with mode %r"
msgstr ""

#: ../../../../nix/store/3diglh51dp3wbayycz40wixk145i03w2-python3-3.10.14/lib/python3.10/argparse.py
#, python-format
msgid "can't open '%(filename)s': %(error)s"
msgstr ""

#: ../../../../nix/store/3diglh51dp3wbayycz40wixk145i03w2-python3-3.10.14/lib/python3.10/argparse.py
#, python-format
msgid "cannot merge actions - two groups are named %r"
msgstr ""

#: ../../../../nix/store/3diglh51dp3wbayycz40wixk145i03w2-python3-3.10.14/lib/python3.10/argparse.py
msgid "'required' is an invalid argument for positionals"
msgstr ""

#: ../../../../nix/store/3diglh51dp3wbayycz40wixk145i03w2-python3-3.10.14/lib/python3.10/argparse.py
#, python-format
msgid ""
"invalid option string %(option)r: must start with a character "
"%(prefix_chars)r"
msgstr ""

#: ../../../../nix/store/3diglh51dp3wbayycz40wixk145i03w2-python3-3.10.14/lib/python3.10/argparse.py
#, python-format
msgid "dest= is required for options like %r"
msgstr ""

#: ../../../../nix/store/3diglh51dp3wbayycz40wixk145i03w2-python3-3.10.14/lib/python3.10/argparse.py
#, python-format
msgid "invalid conflict_resolution value: %r"
msgstr ""

#: ../../../../nix/store/3diglh51dp3wbayycz40wixk145i03w2-python3-3.10.14/lib/python3.10/argparse.py
#, python-format
msgid "conflicting option string: %s"
msgid_plural "conflicting option strings: %s"
msgstr[0] ""
msgstr[1] ""

#: ../../../../nix/store/3diglh51dp3wbayycz40wixk145i03w2-python3-3.10.14/lib/python3.10/argparse.py
msgid "mutually exclusive arguments must be optional"
msgstr ""

#: ../../../../nix/store/3diglh51dp3wbayycz40wixk145i03w2-python3-3.10.14/lib/python3.10/argparse.py
msgid "positional arguments"
msgstr ""

#: ../../../../nix/store/3diglh51dp3wbayycz40wixk145i03w2-python3-3.10.14/lib/python3.10/argparse.py
msgid "options"
msgstr ""

#: ../../../../nix/store/3diglh51dp3wbayycz40wixk145i03w2-python3-3.10.14/lib/python3.10/argparse.py
msgid "show this help message and exit"
msgstr ""

#: ../../../../nix/store/3diglh51dp3wbayycz40wixk145i03w2-python3-3.10.14/lib/python3.10/argparse.py
msgid "cannot have multiple subparser arguments"
msgstr ""

#: ../../../../nix/store/3diglh51dp3wbayycz40wixk145i03w2-python3-3.10.14/lib/python3.10/argparse.py
#, python-format
msgid "unrecognized arguments: %s"
msgstr ""

#: ../../../../nix/store/3diglh51dp3wbayycz40wixk145i03w2-python3-3.10.14/lib/python3.10/argparse.py
#, python-format
msgid "not allowed with argument %s"
msgstr ""

#: ../../../../nix/store/3diglh51dp3wbayycz40wixk145i03w2-python3-3.10.14/lib/python3.10/argparse.py
#, python-format
msgid "ignored explicit argument %r"
msgstr ""

#: ../../../../nix/store/3diglh51dp3wbayycz40wixk145i03w2-python3-3.10.14/lib/python3.10/argparse.py
#, python-format
msgid "the following arguments are required: %s"
msgstr ""

#: ../../../../nix/store/3diglh51dp3wbayycz40wixk145i03w2-python3-3.10.14/lib/python3.10/argparse.py
#, python-format
msgid "one of the arguments %s is required"
msgstr ""

#: ../../../../nix/store/3diglh51dp3wbayycz40wixk145i03w2-python3-3.10.14/lib/python3.10/argparse.py
msgid "expected one argument"
msgstr ""

#: ../../../../nix/store/3diglh51dp3wbayycz40wixk145i03w2-python3-3.10.14/lib/python3.10/argparse.py
msgid "expected at most one argument"
msgstr ""

#: ../../../../nix/store/3diglh51dp3wbayycz40wixk145i03w2-python3-3.10.14/lib/python3.10/argparse.py
msgid "expected at least one argument"
msgstr ""

#: ../../../../nix/store/3diglh51dp3wbayycz40wixk145i03w2-python3-3.10.14/lib/python3.10/argparse.py
#, python-format
msgid "expected %s argument"
msgid_plural "expected %s arguments"
msgstr[0] ""
msgstr[1] ""

#: ../../../../nix/store/3diglh51dp3wbayycz40wixk145i03w2-python3-3.10.14/lib/python3.10/argparse.py
#, python-format
msgid "ambiguous option: %(option)s could match %(matches)s"
msgstr ""

#: ../../../../nix/store/3diglh51dp3wbayycz40wixk145i03w2-python3-3.10.14/lib/python3.10/argparse.py
#, python-format
msgid "unexpected option string: %s"
msgstr ""

#: ../../../../nix/store/3diglh51dp3wbayycz40wixk145i03w2-python3-3.10.14/lib/python3.10/argparse.py
#, python-format
msgid "%r is not callable"
msgstr ""

#: ../../../../nix/store/3diglh51dp3wbayycz40wixk145i03w2-python3-3.10.14/lib/python3.10/argparse.py
#, python-format
msgid "invalid %(type)s value: %(value)r"
msgstr ""

#: ../../../../nix/store/3diglh51dp3wbayycz40wixk145i03w2-python3-3.10.14/lib/python3.10/argparse.py
#, python-format
msgid "invalid choice: %(value)r (choose from %(choices)s)"
msgstr ""

#: ../../../../nix/store/3diglh51dp3wbayycz40wixk145i03w2-python3-3.10.14/lib/python3.10/argparse.py
#, python-format
msgid "%(prog)s: error: %(message)s\n"
msgstr ""

