Þ          ü               Ü  U   Ý     3     O     h     m  Ê   }  
   H  
   S     ^     d     k            D  ¬  (   ñ          1     K     Q  	   W     a     m  	   ö  <      *   =     h  &     ©  ©  W   S  0   «  *   Ü     	  	   	  	  	     "
     /
     K
     X
  !   e
  '   
  1   ¯
  £  á
  =        Ã     Þ     ù               ,  £   9  	   Ý     ç            %   4   Append these arguments to external command invocation when starting the next Pomodoro Custom audio file for alarm Display version and exit Exit Have a nice day Help
====
Start      | {start}
Pause      | {pause}
Reset      | {reset}
Reset All  | {reset_all}
Help       | {help}
Focus Prev | {focus_previous}
Focus Next | {focus_next}
Exit       | {exit_clicked}
 Long break Mute alarm Pause Paused Play alarm and exit Playing alarm once... Playing alarm twice... Please refer to the terms of the LICENSES included with Pomoglorbo.

Curious about how to use Pomoglorbo? Read my blog post: https://www.justus.pw/posts/2024-06-18-try-pomoglorbo.html

Stay up to date with changes: https://codeberg.org/justusw/Pomoglorbo

Reach out to me if you have any questions: justus@jwpconsulting.net
 Pomoglorbo: TUI Pomodoro Technique Timer Press [start] to begin Press [start] to continue Ready Reset Reset All Small break Use a different config file. Overrides POMOGLORBO_CONFIG_FILE environment variable. Default is "$XDG_CONFIG_HOME/pomoglorbo/config.ini". Work mode conflicting option string: %s conflicting option strings: %s expected %s argument expected %s arguments {minutes:02}:{seconds:02} {}{minutes}min {seconds:02}s remaining Project-Id-Version: Pomoglorbo 2024.6.23
Report-Msgid-Bugs-To: justus@jwpconsulting.net
POT-Creation-Date: 2024-06-23 23:19+0900
PO-Revision-Date: 2024-06-23 13:37+0900
Last-Translator: Justus Perlwitz <justus@jwpconsulting.net>
Language: ja
Language-Team: ja <LL@li.org>
Plural-Forms: nplurals=1; plural=0;
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.15.0
 ãã¢ãã¼ã­ãéå§ããéã«å¤é¨ã³ãã³ãå¼ã³åºãã«ã¤ããæå­å ã¢ã©ã¼ã é³ã®ãã¡ã¤ã«ãæå®ã§ãã ãã¼ã¸ã§ã³æå ±ãè¡¨ç¤ºãã¦çµäº çµäº ã¾ãã­ ãã«ã
======
éå§           | {start}
ä¸æåæ­¢       | {pause}
ãªã»ãã       | {reset}
ãã¹ã¦ãªã»ãã | {reset_all}
ãã«ã         | {help}
å             | {focus_previous}
æ¬¡             | {focus_next}
çµäº           | {exit_clicked}
 é·ãä¼æ¯ ã¢ã©ã¼ã ããã¥ã¼ã ä¸æåæ­¢ ä¸æåæ­¢ ã¢ã©ã¼ã ãåçãã¦çµäº ã¢ã©ã¼ã ãåçãã¦ãã¾ã... ã¢ã©ã¼ã ããã1åº¦åçãã¦ãã¾ã... Pomoglorboã«å«ã¾ããLICENSESãã©ã«ãã«å¥ã£ã¦ããã©ã¤ã»ã³ã¹æ¡é ããç¢ºèªãã ããã

Pomoglorboããã£ã¨ãã¾ãä½¿ãããäººã¯ããã¡ãã®ãã­ã°è¨äºããããããã¾ã:https://www.justus.pw/posts/2024-06-18-try-pomoglorbo.html

ææ°æå ±: https://codeberg.org/justusw/Pomoglorbo

ä¸æãªç¹ããã°ããæ°è»½ã«é£çµ¡ãã¦ãã ãã: justus@jwpconsulting.net
 Pomoglorbo: TUI ãã¢ãã¼ã­ã»ãã¯ããã¯ã¿ã¤ãã¼ [éå§]ãæ¼ãã¦éå§ [éå§]ãæ¼ãã¦ç¶è¡ éå§ã§ãã¾ã ãªã»ãã å¨ã¦ãªã»ãã ç­ãä¼æ¯ å¥ã®è¨­å®ãã¡ã¤ã«ãæå®ãããç°å¢å¤æ°ã®POMOGLORBO_CONFIG_FILEãä¸æ¸ããããããã©ã«ãã¯ "$XDG_CONFIG_HOME/pomoglorbo/config.ini" ã ä½æ¥­ä¸­ conflicting option string: %s expected %s argument {minutes:02}:{seconds:02} {} æ®ã{minutes}å {seconds:02}ç§ 