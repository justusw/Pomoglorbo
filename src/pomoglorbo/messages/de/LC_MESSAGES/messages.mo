��    6      �              |     }     �  1   �     �  U   �     N     j     �     �  �   �  
   c  
   n     y          �  D  �  (   �               9     ?  	   E     O  	   [  4   e     �  $   �  (   �  .     <   1  %   n  *   �     �     �     �     	  !   /	  3   Q	  %   �	  N   �	  -   �	     (
  #   E
     i
     q
     �
  (   �
     �
  5   �
     "     =     E  &   _  �  �     7     V  8   v     �  Q   �     !  (   ?     h     p  �   �     C     P     c     i  #   r  n  �  :        @  $   _     �     �  	   �     �     �  F   �     �  ,     ?   B  5   �  V   �  (     (   8     a  #   �     �      �  $   �  3      (   4  X   ]  ;   �  &   �  %        ?     H  ,   ^  ,   �  $   �  :   �          1     =  "   W   %(prog)s: error: %(message)s
 %r is not callable 'required' is an invalid argument for positionals .__call__() not defined Append these arguments to external command invocation when starting the next Pomodoro Custom audio file for alarm Display version and exit Exit Have a nice day Help
====
Start      | {start}
Pause      | {pause}
Reset      | {reset}
Reset All  | {reset_all}
Help       | {help}
Focus Prev | {focus_previous}
Focus Next | {focus_next}
Exit       | {exit_clicked}
 Long break Mute alarm Pause Paused Play alarm and exit Please refer to the terms of the LICENSES included with Pomoglorbo.

Curious about how to use Pomoglorbo? Read my blog post: https://www.justus.pw/posts/2024-06-18-try-pomoglorbo.html

Stay up to date with changes: https://codeberg.org/justusw/Pomoglorbo

Reach out to me if you have any questions: justus@jwpconsulting.net
 Pomoglorbo: TUI Pomodoro Technique Timer Press [start] to begin Press [start] to continue Ready Reset Reset All Small break Work mode ambiguous option: %(option)s could match %(matches)s argument "-" with mode %r can't open '%(filename)s': %(error)s cannot have multiple subparser arguments cannot merge actions - two groups are named %r conflicting option string: %s conflicting option strings: %s dest= is required for options like %r expected %s argument expected %s arguments expected at least one argument expected at most one argument expected one argument ignored explicit argument %r invalid %(type)s value: %(value)r invalid choice: %(value)r (choose from %(choices)s) invalid conflict_resolution value: %r invalid option string %(option)r: must start with a character %(prefix_chars)r mutually exclusive arguments must be optional not allowed with argument %s one of the arguments %s is required options positional arguments show this help message and exit the following arguments are required: %s unexpected option string: %s unknown parser %(parser_name)r (choices: %(choices)s) unrecognized arguments: %s usage:  {minutes:02}:{seconds:02} {}{minutes}min {seconds:02}s remaining Project-Id-Version: Pomoglorbo 2024.6.23
Report-Msgid-Bugs-To: justus@jwpconsulting.net
POT-Creation-Date: 2024-06-23 23:19+0900
PO-Revision-Date: 2024-06-23 13:37+0900
Last-Translator: Justus Perlwitz <justus@jwpconsulting.net>
Language: de
Language-Team: de <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1);
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.15.0
 %(prog)s: Fehler: %(message)s
 %r kann nicht aufgerufen werden 'required' ist ein ungültiges Argument für Positionale .__call__() ist nicht definiert Hänge diese Argumente an jeden externen Befehlsaufruf wenn ein Pomodoro anfängt Wähle Audio-Datei für Alarm Zeige Version an und beende das Programm Beenden Schönen Tag noch Hilfe
Start     | {start}
Pause     | {pause}
Neustart  | {reset}
Neustart+ | {reset_all}
Hilfe     | {help}
Zurück    | {focus_previous}
Vorwärts  | {focus_next}
Beenden   | {exit_clicked}
 Große Pause Spiel keinen Alarm Pause Pausiert Spiel Alarm und beende das Programm Bitte schau in die Lizenzbedingungen im LICENSES Ordner, der in Pomoglorbo enthalten ist.

Möchtest du mehr über Pomoglorbo lernen? Schau gerne in meinem Blog nach: https://www.justus.pw/posts/2024-06-18-try-pomoglorbo.html

Halt dich auf dem Laufenden: https://codeberg.org/justusw/Pomoglorbo

Für Fragen stehe ich gerne zur Verfügung: justus@jwpconsulting.net
 Pomoglorbo: Eine Pomodoro-Uhr als Text-Nutzerschnittstelle [Start] drücken um anzufangen [Start] drücken um weiter zu machen Bereit Neustart Neustart+ Kleine Pause Arbeiten Mehrdeutige Option: %(option)s könnte mit %(matches)s übereinstimmen Argument "-" mit Modus %r Kann nicht '%(filename)s' öffnen: %(error)s Es können nicht mehrere Unterparser-Argumente angegeben werden Kann nicht Aktionen mischen - Zwei Gruppen heißen %r Widersprüchliche Options-Zeichenfolge: %s Widersprüchliche Options-Zeichenfolgen: %s dest= ist notwendig für Optionen wie %r Erwarte %s Argument Erwarte %s Argumente Erwarte mindestens ein Argument Erwarte nicht mehr als ein Argument Erwarte ein Argument Ignoriere explizites Argument %r Ungültiger Wert %(type)s: %(value)r Ungültige Wahl: %(value)r (wähle aus %(choices)s) Ungültiger conflict_resolution Wert: %r Ungültige Zeichenfolge %(option)r: Diese muss mit dem Zeichen %(prefix_chars)r anfangen Gegenseitig ausschließende Argumente müssen optional sein Nicht zusammen mit Argument %s erlaubt Eines der Argumente %s wird benötigt Optionen Positionale Argumente Zeige diese Hilfe an und beende das Programm Die folgenden Argumente werden benötigt: %s Unerwartete Options-Zeichenfolge: %s Unbekannter parser %(parser_name)r (Möglich: %(choices)s) Unbekannte Argumente: %s Benutzung:  {minutes:02}:{seconds:02} {} Noch {minutes}min {seconds:02}s 