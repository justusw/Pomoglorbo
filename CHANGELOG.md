---
title: Changelog
---
<!--
SPDX-FileCopyrightText: 2024 Justus Perlwitz

SPDX-License-Identifier: MIT
-->

# Unreleased

- Mark strings for translation using Babel and add translation utility scripts
  in bin/
- Add German translation
- Add basic Japanese translation
- Refactor some of the code used for layout generation
- Improve rendering performance by consolidating text rendering

# 2024.6.23.1

- Add blocking audio play back for TUI (because blocking will stop it from
  rendering)

# 2024.6.23

- Remove `no_clock` and `focus` configuration variables, and `--no-clock` and
  `--focus` CLI arguments. Always show clock, but leave `no_sound` config
  variable `--no-sound` CLI argument, so the sound can still be disabled.
- Halved volume of b15.wav. It was quite loud.
- Removed `volume` config variable.
- Removed `block` boolean argument in `pomoglorbo.core.sound.play()` method

# 2024.06.22a1 (prerelease)

- Swap out pygame for
  [playsound3](https://github.com/sjmikler/playsound3/tree/main).
  Unfortunately, setting the volume is not possible.

# 2024.06.20

- Allow specifying a config file from the CLI. This will take precedence over
  the default XDG_CONFIG_HOME path, or the POMOGLORBO_CONFIG_FILE environment
  variable.
- Clean up and improve help text
- Add back ability to specify audio file (add a test audio file)
- Simplify the way we derive the playback volume
- Fix `--version` returning old value, use importlib.metadata to find out
  current version
- Add useful documentation
- Hide pygame startup message

# 2024.06.19.3

Fixed j/k key not cycling between buttons correctly

# 2024.06.19.2

Fixed up/down key not cycling between buttons correctly
