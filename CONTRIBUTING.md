<!--
SPDX-FileCopyrightText: 2024 Justus Perlwitz
SPDX-FileCopyrightText: 2021-2023 Bhatihya Perera

SPDX-License-Identifier: MIT
-->

Your contributions are very welcome. Please read the below document to get
an overview over how to contribute to Pomoglorbo.

# What to contribute

Here are some ideas on what you can contribute to this project:

- You have found a bug and would like to submit a fix.
- There is a feature you would like to see and you want to submit a patch and
share it with everyone
- You want to add some tests, or refactor code.
- You want to add localization to Pomoglorbo
- You have a suggestion on how to improve documentation and would like submit
your suggestions and changes.

Also, please check out the [TODO.md](TODO.md) file for more ideas.

# Commits

Please write meaningful commit messages. Ask the Pomoglorbo maintainers for
advice.

# Who can contribute?

Everyone is welcome to contribute to this project. Please refer to the [Code of
Conduct](CODE_OF_CONDUCT.md).
