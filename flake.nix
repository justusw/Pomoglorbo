# SPDX-FileCopyrightText: 2023 Justus Perlwitz
# SPDX-FileCopyrightText: 2024 Justus Perlwitz
#
# SPDX-License-Identifier: MIT

{
  description = "Pomoglorbo";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs/24.05";
    poetry2nix = {
      url = "github:nix-community/poetry2nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, flake-utils, poetry2nix }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        # see https://github.com/nix-community/poetry2nix/tree/master#api for more functions and examples.
        pkgs = nixpkgs.legacyPackages.${system};
        # TODO test with Python 3.10
        python = pkgs.python310;
        pythonPackages = pkgs.python310Packages;
        inherit (poetry2nix.lib.mkPoetry2Nix { inherit pkgs; })
          mkPoetryEnv mkPoetryApplication defaultPoetryOverrides;
        pypkgs-build-requirements = {
          pyright = [ "setuptools" ];
          playsound3 = [ "hatchling" ];
        };
        overrides = defaultPoetryOverrides.extend (self: super: { } // (builtins.mapAttrs
          (package: build-requirements: (
            (builtins.getAttr package super).overridePythonAttrs (old: {
              buildInputs = (old.buildInputs or [ ]) ++ (builtins.map (pkg: if builtins.isString pkg then builtins.getAttr pkg super else pkg) build-requirements);
            })
          ))
          pypkgs-build-requirements));
        poetryEnv = mkPoetryEnv {
          projectDir = self;
          inherit overrides;
          inherit python;
          groups = [ "dev" "test" ];
        };
      in
      {
        packages = {
          pomoglorbo = mkPoetryApplication {
            projectDir = self;
            inherit overrides;
            inherit python;
          };
          default = self.packages.${system}.pomoglorbo;
        };

        devShells.default = poetryEnv.env.overrideAttrs (
          oldAttrs: {
            buildInputs = [ pkgs.reuse ];
          }
        );
      });
}
